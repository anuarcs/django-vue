from rest_framework import routers
from tweets.viewsets import TweetsViewSet


router = routers.DefaultRouter()
router.register(r'tweets', TweetsViewSet)