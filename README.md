# Poor Man's Twitter using VUE and Django

"Poor Man's Twitter" application in a single web page. The page consists of 2 pieces of functionality on the URL '/'.

- The ability for any visitor to tweet. A tweet consists of a 50 character text input, a datetime that automatically records the time of a message and a name. Visible form fields for a tweet are the tweet and the name, which must be aligned horizontally to one another.
- The ability to display all tweets (un-paginated) in a table . Show the time of the tweet, the message and the name. Sort the table using only the time and name columns.

## Installing

1. Clone repository
```
   git clone git@gitlab.com:anuarcs/django-vue.git
   ```
   2. Create and activate virtual environment
   ```
   virtualenv project_env
   source project_env/bin/activate
   ```
   3. Move to the project main dir "django-vue" and install requirements
   ```
   pip install -r reqs.txt
   ```
   4. Run migrations
   ```
   python manage.py migrate
   ```
   5. Start development server
   ```
   python manage.py runserver
   ```
   6. Open browser [http:localhost:8000](http://localhost:8000)
   
   # Demo
   Visit: [http://dvue.demasys.net/](http://dvue.demasys.net/)
   