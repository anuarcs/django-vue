from rest_framework.test import APIClient
from rest_framework import status
from django.test import TestCase
from tweets.models import Tweet
from tweets.serializers import TweetSerializer
from unittest import mock
import pytz
import datetime


class TweetTest(TestCase):
    """ Test module for Tweet model """

    def test_auto_now(self):
        mocked = datetime.datetime(2018, 10, 1, 0, 0, 0, tzinfo=pytz.utc)
        with mock.patch('django.utils.timezone.now',
                        mock.Mock(return_value=mocked)):
            tweet_test = Tweet.objects.create(name='Anuar',
                                              tweet="Example tweet")
            self.assertEqual(tweet_test.created, mocked)


# initialize the APIClient app
client = APIClient()


class ApiTweetsTest(TestCase):
    """ Test module for GET all tweets """

    def setUp(self):
        Tweet.objects.create(name='anuarcs', tweet="First tweet")
        Tweet.objects.create(name='anuarcs', tweet="Second tweet")
        Tweet.objects.create(name='anuarcs', tweet="Third tweet")

    def test_get_all_tweets(self):
        # get API response
        response = client.get('/api/tweets/',
                              {'ordering': 'created'},
                              format='json')

        tweets = Tweet.objects.all()
        serializer = TweetSerializer(tweets, many=True)
        self.assertEqual(response.data, serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_create_tweet(self):
        # get API response
        response = client.post('/api/tweets/',
                               {'name': 'anuarcs', 'tweet': 'Test tweet'},
                               format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
