from django.db import models


class Tweet(models.Model):
    """
    Tweet Model
    Used for saving user tweets
    """
    name = models.CharField(max_length=50)
    tweet = models.CharField(max_length=50)
    created = models.DateTimeField(auto_now_add=True)
