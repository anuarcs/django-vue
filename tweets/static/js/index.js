Vue.filter('capitalize', function (value) {
  if (!value) return ''
  value = value.toString()
  return value.charAt(0).toUpperCase() + value.slice(1)
});

new Vue({
  el: '#tweets-app',
  delimiters: ['${','}'],
  sortKey: 'created',
  data: {
    sortKey: 'created',
    ordering: 'created',
    reverse: false,  
    columns: ['name', 'tweet', 'created'],
    newTweet: {'name':null, 'tweet':null},
    tweets: [],
    loading: false,   
    errors: [], 
    error: false,  
  },
  mounted: function(){  	
  	this.getTweets();
  },
  methods: {
    sortBy: function(sortKey) {
      this.reverse = (this.sortKey == sortKey) ? ! this.reverse : false;      
      this.sortKey = sortKey;
      this.ordering = sortKey;
      if (this.reverse)
      	this.ordering = '-' + this.ordering;       	
      this.getTweets();
    },

    getTweets: function() {
	  this.loading = true;
	  this.$http.get('/api/tweets/', {"params": {"ordering": this.ordering}})
	      .then((response) => {
	        this.tweets = response.data;
	        this.loading = false;
	      })
	      .catch((err) => {
	       this.loading = false;
	       console.log(err);
	      });
	 },	 
	 addTweet: function() {
	  this.loading = true;
	  this.$http.post('/api/tweets/',this.newTweet)
	      .then((response) => {
	        this.loading = false;
	        this.error = false;
	        this.getTweets();
	        this.newTweet = {'name':null, 'tweet':null};
	      })
	      .catch((err) => {
	        this.loading = false;
	        this.errors = err.body;
	        this.error = true;
	        $('.alert').alert();
	        console.log(err);
	      })
	 }
  }
});