from rest_framework import viewsets
from rest_framework import filters
from .models import Tweet
from .serializers import TweetSerializer


class TweetsViewSet(viewsets.ModelViewSet):
    """
    Return a list of all the existing tweets.
    """
    queryset = Tweet.objects.all()
    serializer_class = TweetSerializer
    filter_backends = (filters.OrderingFilter,)
